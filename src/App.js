import React from 'react';
import './App.css';

function App() {

  return (
    <div className="App">
      <header>
          <script src="https://cdn.snipcart.com/themes/v3.0.20/default/snipcart.js"></script>

          <h4>Sports Car Online</h4>
          <div>
              <ul className="header-right-part">
                  {/*<li><i className="fas fa-cart-plus"><button></button></i>Cart</li>*/}
                  <a className="header__summary snipcart-checkout snipcart-summary" href="#" style={{textDecoration: "none"}}>
                      <svg width="31" height="27" viewBox="0 0 31 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M1.10512 0.368718C0.560256 0.368718 0.118164 0.812066 0.118164 1.35848C0.118164 1.9049 0.560256 2.34824 1.10512 2.34824H4.90887L8.30138 18.4009C8.43503 19.0053 8.83085 19.5079 9.32946 19.5041H25.7788C26.3005 19.5118 26.7799 19.0375 26.7799 18.5143C26.7799 17.9911 26.3006 17.5168 25.7788 17.5245H10.1315L9.71003 15.545H27.095C27.5371 15.5412 27.9547 15.2048 28.0511 14.7718L30.354 4.87412C30.4825 4.29933 29.9852 3.67172 29.3979 3.66786H7.21171L6.6771 1.15221C6.58329 0.71276 6.15921 0.368652 5.7107 0.368652L1.10512 0.368718ZM7.623 5.64746H12.7634L13.2569 8.61674H8.25005L7.623 5.64746ZM14.7785 5.64746H20.9881L20.4946 8.61674H15.2719L14.7785 5.64746ZM23.0031 5.64746H28.1537L27.4649 8.61674H22.5097L23.0031 5.64746ZM8.67181 10.5963H13.5862L14.0797 13.5656H9.29919L8.67181 10.5963ZM15.6009 10.5963H20.1656L19.6721 13.5656H16.0944L15.6009 10.5963ZM22.1807 10.5963H27.0023L26.3135 13.5656H21.6872L22.1807 10.5963ZM12.6197 20.164C10.8141 20.164 9.32979 21.6525 9.32979 23.4632C9.32979 25.2739 10.8141 26.7624 12.6197 26.7624C14.4252 26.7624 15.9095 25.2739 15.9095 23.4632C15.9095 21.6525 14.4252 20.164 12.6197 20.164ZM22.4892 20.164C20.6837 20.164 19.1994 21.6525 19.1994 23.4632C19.1994 25.2739 20.6837 26.7624 22.4892 26.7624C24.2948 26.7624 25.7791 25.2739 25.7791 23.4632C25.7791 21.6525 24.2948 20.164 22.4892 20.164ZM12.6197 22.1435C13.3586 22.1435 13.9356 22.7222 13.9356 23.4632C13.9356 24.2042 13.3586 24.7829 12.6197 24.7829C11.8807 24.7829 11.3037 24.2042 11.3037 23.4632C11.3037 22.7222 11.8807 22.1435 12.6197 22.1435ZM22.4892 22.1435C23.2282 22.1435 23.8052 22.7222 23.8052 23.4632C23.8052 24.2042 23.2282 24.7829 22.4892 24.7829C21.7503 24.7829 21.1733 24.2042 21.1733 23.4632C21.1733 22.7222 21.7503 22.1435 22.4892 22.1435Z" fill="coral"/>
                      </svg>
                      <span className="header__price snipcart-total-price"></span>
                  </a >


                  <li>Home</li>
                  <li>About</li>
                  <li>Search</li>
              </ul>
          </div>
      </header>




        <div className="container">
            <h1 className="container__content__breadcrumb">
                Need For Speed
            </h1>
            <div className="container__content__title">
                An exclusive collection of sports car available for everyone.
            </div>

            <div className="container_content_product">
                <div className="p1">
                    <img src="https://d3lp4xedbqa8a5.cloudfront.net/s3/digital-cougar-assets/whichcar/2019/06/19/-1/2019-Mercedes-AMG-GT-R-12-hours-review.jpg"/>
                    <ul>
                        <h4 className="p1-name">Mercedes-AMG GT R</h4>
                        <p>Engine type: 4.0 L M178 (Mercedes-AMG) twin-turbocharged V8 6.2 L M159 DOHC V8 (AMG GT3).</p>
                        <div className="checkout">
                            <h5>$190400.00</h5>
                            <button className="snipcart-add-item"
                                    data-item-id="starry-night"
                                    data-item-price="79.99"
                                    data-item-url="/paintings/starry-night"
                                    data-item-description="High-quality replica of The Starry Night by the Dutch post-impressionist painter Vincent van Gogh."
                                    data-item-image="https://d3lp4xedbqa8a5.cloudfront.net/s3/digital-cougar-assets/whichcar/2019/06/19/-1/2019-Mercedes-AMG-GT-R-12-hours-review.jpg"
                                    data-item-name="The Starry Night">
                                Add to cart
                            </button>
                        </div>
                    </ul>
                </div>

                <div className="p1">
                    <img src="https://cdn.motor1.com/images/mgl/JmVR6/s4/2019-audi-r8-onlocation.jpg"/>
                    <ul>
                        <h4 className="p1-name">Audi R8 Coupé</h4>
                        <p>Engine type: V10 / 40V. Max. output 602 hp @ 8100 rpm.</p>
                        <div className="checkout">
                            <h5>$221000.00</h5>
                            <button className="snipcart-add-item"
                                    data-item-id="starry-night"
                                    data-item-price="79.99"
                                    data-item-url="/paintings/starry-night"
                                    data-item-description="High-quality replica of The Starry Night by the Dutch post-impressionist painter Vincent van Gogh."
                                    data-item-image="https://cdn.motor1.com/images/mgl/JmVR6/s4/2019-audi-r8-onlocation.jpg"
                                    data-item-name="The Starry Night">
                                Add to cart
                            </button>
                        </div>
                    </ul>
                </div>

                <div className="p1">
                    <img src="https://www.motortrend.com/uploads/sites/5/2018/09/2018-Porsche-911-GT2-front-three-quarter-in-motion-2.jpg?fit=around%7C875:492"/>
                    <ul>
                        <h4 className="p1-name">Porsche 911 GT2</h4>
                        <p>Engine type: 3.8L flat-six 700 hp and 553 lb-ft of torque.</p>
                        <div className="checkout">
                            <h5>$295000.00</h5>
                            <button className="snipcart-add-item"
                                    data-item-id="starry-night"
                                    data-item-price="79.99"
                                    data-item-url="/paintings/starry-night"
                                    data-item-description="High-quality replica of The Starry Night by the Dutch post-impressionist painter Vincent van Gogh."
                                    data-item-image="https://www.motortrend.com/uploads/sites/5/2018/09/2018-Porsche-911-GT2-front-three-quarter-in-motion-2.jpg?fit=around%7C875:492"
                                    data-item-name="The Starry Night">
                                Add to cart
                            </button>
                        </div>
                    </ul>
                </div>

                <div className="p1">
                    <img src="https://www.motortrend.com/uploads/sites/5/2018/02/2018-Lamborghini-Huracan-Performante-front-side-view-closer.jpg?fit=around%7C875:492"/>
                    <ul>
                        <h4 className="p1-name">Lamborghini Huracán</h4>
                        <p>Engine type: 5.2L V10 602 hp and 413 lb-ft of torque.</p>
                        <div className="checkout">
                            <h5>$265000.00</h5>
                            <button className="snipcart-add-item"
                                    data-item-id="starry-night"
                                    data-item-price="79.99"
                                    data-item-url="/paintings/starry-night"
                                    data-item-description="High-quality replica of The Starry Night by the Dutch post-impressionist painter Vincent van Gogh."
                                    data-item-image="https://www.motortrend.com/uploads/sites/5/2018/02/2018-Lamborghini-Huracan-Performante-front-side-view-closer.jpg?fit=around%7C875:492"
                                    data-item-name="The Starry Night">
                                Add to cart
                            </button>
                        </div>
                    </ul>
                </div>

                <div className="p1">
                    <img src="https://www.carscoops.com/wp-content/uploads/2019/10/b225c912-mclaren-600lt-comet-fade.jpg"/>
                    <ul>
                        <h4 className="p1-name">McLaren 600LT</h4>
                        <p>Engine type: turbocharged 3.8-liter V8 (592 horsepower, 457 lb-ft of torque).</p>
                        <div className="checkout">
                            <h5>$259000.00</h5>
                            <button className="snipcart-add-item"
                                    data-item-id="starry-night"
                                    data-item-price="79.99"
                                    data-item-url="/paintings/starry-night"
                                    data-item-description="High-quality replica of The Starry Night by the Dutch post-impressionist painter Vincent van Gogh."
                                    data-item-image="https://www.carscoops.com/wp-content/uploads/2019/10/b225c912-mclaren-600lt-comet-fade.jpg"
                                    data-item-name="The Starry Night">
                                Add to cart
                            </button>
                        </div>
                    </ul>
                </div>




            </div>
        </div>
    </div>
  );
}

export default App;
